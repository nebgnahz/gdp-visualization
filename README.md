A Network Visualization Page based [d3](http://d3js.org/).

Since the data needs to be loaded, you need a minimal HTTP server here. The
simplest thing is `python -m SimpleHTTPServer` and then point your browser to
[localhost:8000](http://localhost:8000).

It's (a hack) based on D3. The overall workflow is the following:
1. All GDP routers can be accessed by $IP:15000 for diagnose data.
2. I have a shell script that grabs the data periodically.
3. Then the javascript code will grab the data and show the visualization.

More explanation below.


1. Sample data format looks like:
```
 data/sample.json
 data/sample2.json
```

The list of server IPs are specified in:
```
 data/server
```

2. I use Makefile to document what I did, so `make run` invokes
```
watch -n 30 'cat servers | xargs -n 2 -P 10 sh -c "curl -s $0 >
tmp/sample$1.json"'
```

It's a bit cryptic, but basically every 30 second, a command is
executed. The command grabs a list of servers and use xargs to do
parallel HTTP requests. The results are stored temporarily in "tmp"
folder. You can use “http://explainshell.com/“ to understand what's
going on.

3. The results will be loaded by the javascript 'js/load.js'.
`js/net-vis.js` will construct a graph and show it on the webpage.

Current active webpage:
http://universe.eecs.berkeley.edu:8081/