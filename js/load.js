'use strict';

function padToFour(number) {
    if (number<=9999) { number = ("000"+number).slice(-4); }
    return number;
}

var geoDB = {};

function load_data(all_resolve, all_reject) {
    var data = {};
    data.nodes = [];
    data.links = [];

    var nodesDB = {};
    var linksDB = {};

    function load_from_address(url, resolve, reject) {
        d3.json(url, function(error, sample) {
            if (error) {
                reject(error);
                return;
            }

            if (!sample) {
                reject(url + "is broken");
                return;
            }

            // We consider this path as the primary path, so it could
            // potentially override the previous information.
            var this_pos = add_node(
                sample["My Type"],
                sample["My Public IP"],
                sample["My Private IP"] + ":" + sample["My GDP Port"]
            );

            add_clients(this_pos, sample["Clients"]);

            function add_clients(node_index, clients) {
                var node = data.nodes[node_index];
                var node_id = node.addr;
                if (node_id in nodesDB) {
                    node.clients = clients
                } else {
                    console.log("adding clients to non-existing node, error!");
                }
            }

            function add_node(type, proxy, addr) {
                var node = { type: type, proxy: proxy, addr: addr };
                var node_id = addr;
                if (!(node_id in nodesDB)) {
                    console.log("adding " + node_id)
                    nodesDB[node_id] = true;
                    console.log(JSON.stringify(nodesDB));

                    data.nodes.push(node);
                }

                var keys = Object.keys(nodesDB);
                return keys.indexOf(node_id);
            }

            function add_link(from, to, bytes) {
                var link = { from: from, to: to, bytes: bytes };
                var link_id = padToFour(from) + "->" + padToFour(to);
                if (!(link_id in linksDB)) {
                    //console.log("adding " + link_id)
                    linksDB[link_id] = true;
                    data.links.push(link);
                }
            };

            // building links
            // use this_pos.
            for (var i = 0; i < sample["Primary Nodes"].length; i++) {
                var node = sample["Primary Nodes"][i];
                // add links to primary node
                var other_addr = node["NodeID (IP:port)"];
                var other_pos = add_node("Primary",
                                         other_addr.substr(0, other_addr.indexOf(':')),
                                         node["NodeID (IP:port)"]);
                add_link(this_pos, other_pos, node["Outbound Throughput (bps) "]);
                add_link(other_pos, this_pos, node["Inbound Throughput (bps) "]);
            }

            for (var i = 0; i < sample["Secondary Nodes"].length; i++) {
                var s_nodes = sample["Secondary Nodes"][i];
                var proxy = s_nodes["NATIP"];
                for (var j = 0; j < s_nodes["Nodes"].length; j++) {
                    var node = s_nodes["Nodes"][j];
                    var other_pos = add_node(
                        "Secondary", proxy,
                        node["NodeID (IP:port)"]);

                    add_link(this_pos, other_pos,
                             node["Outbound Throughput (bps) "]);
                    add_link(other_pos, this_pos,
                             node["Inbound Throughput (bps) "]);
                }
            }

            resolve(url);
        });
    }


    var reqs = ["data/tmp/sample1.json",
                "data/tmp/sample2.json",
                "data/tmp/sample3.json",
                "data/tmp/sample4.json",
                "data/tmp/sample5.json",
                "data/tmp/sample6.json",
                "data/tmp/sample7.json"];
    var num_request = reqs.length;

    reqs.map(function(i) {
        var promise = new Promise(function(resolve, reject) {
            load_from_address(i, resolve, reject);
        }).then(function(val) {
            num_request--;
            if (num_request === 0) {
                all_resolve(data);
            }
        }).catch(function(reason) {
            num_request--;
            console.log(reason);
            if (num_request === 0) {
                all_resolve(data);
            }
        });
    });
}

Array.prototype.unique = function() {
    var unique = [];
    for (var i = 0; i < this.length; i++) {
        if (unique.indexOf(this[i]) == -1) {
            unique.push(this[i]);
        }
    }
    return unique;
};

function geo_lookup(ips, all_resolve, all_reject) {
    var finished = 0;
    for (var i = 0; i < ips.length; i++) {
        var promise = new Promise(function(resolve, reject) {
            var ip = ips[i];
            d3.json("http://ipinfo.io/" + ip + "/json", function(error, ipinfo) {
                if (error) reject(error);

                var loc = ipinfo.loc.split(",");
                loc.push(ipinfo.hostname);
                geoDB[ip] = loc;
                finished++;
                resolve(loc);
            });
        });
        promise
            .then(function(val) {
                // check if we have finished
                if (finished === ips.length) {
                    // we return the final promise
                    all_resolve(geoDB);
                }
            })
            .catch(function(reason) {
                all_reject(reason);
            });
    }
}

var graph;
function data_resolved(data) {
    d3.selectAll(".label").remove()
    d3.selectAll(".node").remove()
    d3.selectAll(".link").remove()

    var geo_promise = new Promise(function(resolve, reject) {
        var ips = data.nodes.map(function(node) {
            return node.proxy;
        }).unique();

        geo_lookup(ips, resolve, reject);
    });

    geo_promise
        .then(function(val) {
            // we got all, then we assign to data.
            for (var i = 0; i < data.nodes.length; i++) {
                var ip = data.nodes[i].proxy;
                var loc = val[ip];
                data.nodes[i].lat = loc[0];
                data.nodes[i].long = loc[1];
                data.nodes[i].hostname = loc[2];
            }

            graph = data;
            for (var j = 0; j < graph.links.length; j++) {
                var link = graph.links[j];
                link.from = graph.nodes[link.from];
                link.to = graph.nodes[link.to];
            }

            graph.nodes.sort(function(a, b) {
                return a.hostname.localeCompare(b.hostname);
            });

            draw_net(graph);
        })
        .catch(function(reason) {
            // error, alert
            alert(reason);
        });

}

function all_reject(reason) {
    console.log(reason);
}

load_data(data_resolved, all_reject);

// var intervalID = setInterval(function(){
//     load_data(data_resolved, all_reject);
// }, 5000);
